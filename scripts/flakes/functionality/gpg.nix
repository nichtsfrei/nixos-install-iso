{ pkgs, ... }: {
  programs.gnupg.agent.enable = true;
  services.pcscd.enable = true; # needed for card reader
}
