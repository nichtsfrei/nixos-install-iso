{ stdenv, lib}:
stdenv.mkDerivation rec {
  name = "format-scripts";
  src = ./.;
  installPhase = ''
    mkdir --parents "$out/bin"
    mkdir --parents "$out/usr/local/src"
    chmod +x test.sh
    cp ./test.sh "$out/bin"
    cp -r flakes "$out/"
  '';


}
