{ pkgs, ... }: {
  programs.tmux = {
    enable = true;
    shortcut = "a";
    aggressiveResize = true;
    newSession = true;
    # Stop tmux+escape craziness.
    escapeTime = 0;

    extraConfig = ''
      set-option -g mouse on
      set -g renumber-windows on
      set -g history-limit 10000

      set -g window-style 'fg=colour247,bg=colour236'
      set -g window-active-style 'fg=white,bg=black'

      set -g pane-border-style 'fg=colour235,bg=colour238' 
      set -g pane-active-border-style 'fg=colour51,bg=colour236'
      set-window-option -g mode-keys vi
      bind k selectp -U # switch to panel Up
      bind j selectp -D # switch to panel Down 
      bind h selectp -L # switch to panel Left
      bind l selectp -R # switch to panel Right

      set -g default-terminal "screen-256color"

      set -g status-bg '#005577' 
      set -g status-fg white

      set-window-option -g window-status-current-format '#[fg=white,bold] | #{window_index} #[fg=white]#{pane_current_command} #[fg=black]#(echo "#{pane_current_path}" | rev | cut -d'/' -f-3 | rev) #[fg=white] |'
      set-window-option -g window-status-format '#[fg=white,bold]#{window_index} #[fg=white]#{pane_current_command} #[fg=black]#(echo "#{pane_current_path}" | rev | cut -d'/' -f-3 | rev) #[fg=white]|'
    '';
  };
}
