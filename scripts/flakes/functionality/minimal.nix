{ lib, config, pkgs, ... }: {
  programs.neovim = {
    enable = true;
  };

  environment.systemPackages = with pkgs; [
    curl
    git
    lsof
    pciutils
    gnupg
    python3
  ];
}
