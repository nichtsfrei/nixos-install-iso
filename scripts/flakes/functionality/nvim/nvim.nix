{ config, pkgs, lib, ... }: {

  environment.variables = { EDITOR = "vim"; };

  environment.systemPackages = with pkgs; [
    (neovim.override {
      vimAlias = true;
      configure = {
        packages.myPlugins = with pkgs.vimPlugins; {
          start = [ vim-nix ];
          opt = [ ];
        };
        customRC = builtins.concatStringsSep "\n" [
          (lib.strings.fileContents ./theme.vim)
          (lib.strings.fileContents ./base.vim)
        ];
      };
    }
    )
  ];



}
