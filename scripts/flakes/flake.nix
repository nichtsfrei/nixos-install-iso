{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = inputs@{ self, nixpkgs, ... }:
    let
      system = "x86_64-linux";
      mkHomeMachine = configurationNix: extraModules: nixpkgs.lib.nixosSystem {
        inherit system;
        # Arguments to pass to all modules.
        specialArgs = { inherit system inputs; };
        modules = ([
          # System configuration
          ./configuration.nix
          # isn't that already in configurationNix?
          ./functionality/minimal.nix
          ./functionality/docker.nix
          ./functionality/virtualisation.nix
          ./functionality/tmux.nix
          ./functionality/nvim/nvim.nix
        ] ++ extraModules);
      };
    in
    {
      nixosConfigurations.ratiospatz = mkHomeMachine
        ./host/ratiospatz.nix
        [
          ./functionality/gpg.nix
          ./functionality/desktop.nix
          ./functionality/steam.nix
          ./user/philipp.nix
          ./host/ratiospatz.nix
        ];
    };

}
