{ pkgs ? import <nixpkgs> { }}:

let
  scripts = pkgs.callPackage ./scripts/default.nix { };
in
  pkgs.mkShell {
    buildInputs = [
      scripts
      pkgs.shfmt
    ];
  }
