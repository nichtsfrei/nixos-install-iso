# nixos-auto-install

A semi-automatiic highly-opinionated way to install [ NixOS ]
  (https://nixos.org/).

**WARNING: Booting this ISO is intended to wipe a disk on the system. DO NOT plug it into any device that has important data.**

The created image will lok for hdd devices:

1. /dev/sdb

then partioning and formating found block-device; it also initializes a luks encryption with `/dev/zero` and keysize one.

To verify if the preparation is done check: `journalctl -fb -n100 -uinstall`
To set proper encryption either set a password by calling

```
cryptsetup luksChangeKey /dev/disk/by-label/root --key-file=/dev/zero --keyfile-size=1

```

or chang eit by other means. Afterwards you should select and install a flake by calling:

```
nixos-install --flake /mnt/etc/nixos#ratiospatz 
```

All three commands should be in the history.

# ToDo

1. remove automatic destroying of system in favor of a shell script
1. provide documentation
