{ config, pkgs, lib, ... }: {
  zramSwap.enable = true;
  hardware.enableAllFirmware = true;
  system.autoUpgrade.enable = true;

  boot.initrd.kernelModules = [ "i915" ];
  boot.kernelModules = [ "acpi_call" ];
  boot.extraModulePackages = with config.boot.kernelPackages; [ acpi_call ];
  boot.initrd.availableKernelModules = [
    "nvme"
    "xhci_pci"
  ];

  services.throttled.enable = lib.mkDefault true;

  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  hardware.opengl.extraPackages = with pkgs; [
    vaapiIntel
    vaapiVdpau
    libvdpau-va-gl
    intel-media-driver
  ];

  networking.networkmanager.enable = true;
  networking.hostName = "ratiospatz";

  services.openssh.enable = true;
  users.mutableUsers = true;
}
