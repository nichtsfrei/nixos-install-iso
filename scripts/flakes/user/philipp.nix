{ pkgs, ... }: {
  programs.fish.enable = true;
  programs.fish.promptInit = ''
    any-nix-shell fish --info-right | source
  '';
  users.extraUsers.philipp = {
    isNormalUser = true;
    extraGroups = [ "networkmanager" "wheel" "audio" "cdrom" "docker" "libvirtd" ];
    uid = 1000;
    shell = pkgs.fish;

  };

  users.users.philipp.packages = with pkgs;[
    pass
    firefox
    nodejs
    tree-sitter
    nodePackages.prettier
    nodePackages.pyright
    gopls
    rust-analyzer
    ccls
    any-nix-shell
    s-tui
    stress

    (neovim.override {
      vimAlias = true;
      configure = {
        packages.myPlugins = with pkgs.vimPlugins; {
          start = [
            vim-nix
            coc-nvim
            coc-pyright
            coc-go
          ];
          opt = [ ];
        };
        customRC = builtins.concatStringsSep "\n" [
          (lib.strings.fileContents ./../functionality/nvim/theme.vim)
          (lib.strings.fileContents ./../functionality/nvim/base.vim)
          (lib.strings.fileContents ./coc.vim)
        ];
      };
    }
    )

  ];

}
