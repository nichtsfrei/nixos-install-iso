#!/bin/sh

SCIPT_PATH="$(dirname $(readlink -f $0))"
set -eux
dev=/dev/sda
[ -b /dev/nvme0n1 ] && dev=/dev/nvme0n1
[ -b /dev/vda ] && dev=/dev/vda
sfdisk --wipe=always $dev <<-END
	label: gpt
	name=BOOT, size=512MiB, type=C12A7328-F81F-11D2-BA4B-00A0C93EC93B
	name=NIXOS
END
sync
until [ -b /dev/disk/by-partlabel/BOOT ]; do sleep 1 ; done
ls -las /dev/disk/by-partlabel
mkfs.fat -F 32 -n boot /dev/disk/by-partlabel/BOOT

sync
cryptsetup luksFormat --type=luks2 --label=root /dev/disk/by-partlabel/NIXOS /dev/zero --keyfile-size=1
cryptsetup luksOpen /dev/disk/by-partlabel/NIXOS root --key-file=/dev/zero --keyfile-size=1
mkfs.ext4 -L nixos /dev/mapper/root

sync
until [ -b /dev/disk/by-label/boot ]; do sleep 1 ; done

mount /dev/mapper/root /mnt
mkdir -p /mnt/etc
mkdir /mnt/boot
mount /dev/disk/by-label/boot /mnt/boot

cp -r $SCIPT_PATH/../flakes /mnt/etc/nixos

printf 'Preparation done; please set correct luks password by:\n'
printf 'cryptsetup luksChangeKey /dev/disk/by-label/root --key-file=/dev/zero --keyfile-size=1\n'
printf '\t and install your flake by:\n'
printf 'nixos-install --flake FLAKE\n'
printf "Hallo\n"
