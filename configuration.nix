{ config, lib, pkgs, ... }: {
  nixpkgs.config.allowUnfree = true;
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  zramSwap.enable = true;
  services.logind.lidSwitch = "ignore";

  security.sudo.wheelNeedsPassword = false;

  networking.hostName = "unknownsparrow";

  services.openssh.enable = true;
  services.openssh.permitRootLogin = "yes";

  users.mutableUsers = true;
  users.users.root = {
    # Password is "linux"
    hashedPassword = lib.mkForce "$6$7IKExnDde920x.YH$ggegnnKJYdmg1Wt33fxuPpM.MmIaX32LXVyjL8ed7ohT385lKotFGzRpitncQ3pd9Lci1QCFGRn2tVJGxkFAm0";
  };

  services.avahi = {
    enable = true;
    ipv4 = true;
    ipv6 = true;
    nssmdns = true;
    publish = { enable = true; domain = true; addresses = true; };
  };

  environment.systemPackages = let 
    scripts = pkgs.callPackage ./scripts/default.nix { };
  in
    with pkgs; [
    scripts
    curl
    neovim
    git
    lsof
    pciutils
    tmux
  ];
}
