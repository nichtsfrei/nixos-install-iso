" Use <c-space> for trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

nnoremap <silent> Q :call CocActionAsync('doQuickfix')<CR>
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')
set statusline^=%{coc#status()}
let g:coc_user_config = {
\  'languageserver': {
\    'ccls': {
\      'command': 'ccls',
\      'filetypes': [
\        'c',
\        'cpp',
\        'objc',
\        'objcpp'
\      ],
\      'rootPatterns': [
\        '.ccls',
\        'compile_commands.json',
\        '.vim\',
\        '.git\',
\        '.hg\'
\      ],
\      'initializationOptions': {
\        'cache': {
\          'directory': '/tmp/ccls'
\        }
\      }
\    }
\  }
\}
